const  os=require('os');
const doc=require('fs');

console.log("Folder  Path");
var docu=doc.readdirSync("C:\\Users\\pc\\Desktop\\html pages",['utf8']);
console.log(docu);


console.log("\n\nThumbnail");
const thumnail=require('thumbsupply');
const { Console } = require('console');

var thumb = thumnail.generateThumbnail('Node_Architecture_Mosh.mp4', {
    size: thumnail.ThumbSize.MEDIUM, // or ThumbSize.LARGE
    timestamp: "10%", // or `30` for 30 seconds
    forceCreate: true,
    cacheDir: "~/myapp/cache",
    mimetype: "video/mp4"
})

var img=doc.readdirSync("D:\\Febapp\\~\\myapp\\cache",['utf8']);
console.log(img);

console.log("\n\nFuzzy Search .... ");
var arr = ["vitra","pitney","era","pinaki","deloitte","Paytm","software","google","apple"];
var val="itvar";
if(arr.includes(val)) {
    console.log(val);
} else {
    var s1 = calculateHash(val);
    for(var i=0; i<arr.length; i++) {
        var s2 = calculateHash(arr[i]);

        if(s1 === s2){
            console.log(arr[i]);
        }
    }
}

function calculateHash(word) {
    var sum = 0;
    for(var i=0; i<word.length; i++) {
        var ch = word[i];
        var val=ch.charCodeAt(0);
        sum = sum + val;
    }
    return sum;
}

console.log("\n\nMemory Usage");
var fm=os.freemem();
var tm=os.totalmem();
var total=tm-fm;
console.log(total);

console.log("\n\nCpu usage");;
var cpus=os.cpus();
var len = cpus.length;
var core="";
for(var i = 0; i < len; i++) {
    core += "{ ";
    var cpu = cpus[i], total = 0;
    for(var type in cpu.times) {
        total += cpu.times[type];
    }

    for(type in cpu.times) {
        core += type + ":" + Math.round(100 * cpu.times[type] / total) + " ";
    }
    core += "  } \n";
}
console.log(core);